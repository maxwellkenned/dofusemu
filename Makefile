build: Server/Game/target/game-1.0-jar-with-dependencies.jar Server/Login/target/login-1.0-jar-with-dependencies.jar docker/game/game.jar docker/login/login.jar
	docker-compose build

clean:
	mvn -f Server/Game/pom.xml clean
	mvn -f Server/Login/pom.xml clean
	rm docker/game/game.jar
	rm docker/login/login.jar

init:
	docker-compose exec db mysql -uroot -proot -e "create database game;"
	docker-compose exec db mysql -uroot -proot -e "create database login;"
	docker-compose exec db mysql -uroot -proot -e "use game;source /var/www/html/game.sql;"
	docker-compose exec db mysql -uroot -proot -e "use login;source /var/www/html/login.sql;"


Server/Game/target/game-1.0-jar-with-dependencies.jar:
	mvn -f Server/Game/pom.xml compile assembly:single

Server/Login/target/login-1.0-jar-with-dependencies.jar:
	mvn -f Server/Login/pom.xml compile assembly:single


docker/game/game.jar: Server/Game/target/game-1.0-jar-with-dependencies.jar
	cp Server/Game/target/game-1.0-jar-with-dependencies.jar docker/game/game.jar

docker/login/login.jar: Server/Login/target/login-1.0-jar-with-dependencies.jar
	cp Server/Login/target/login-1.0-jar-with-dependencies.jar docker/login/login.jar

